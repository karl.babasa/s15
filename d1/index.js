console.log("hello World");


// Assignment Operator (=)

let assignmentNumber = 8;


// Addition assignment operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

assignmentNumber += 2;
console.log(assignmentNumber); //12

// Subtraction/Multiplication/Division assignment operator (-=, *=, /=)
assignmentNumber -= 2;
assignmentNumber *= 2;
assignmentNumber /= 2;

// Arithmetic Operators (+, -, *, /, %)

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operator" + mdas);

let pemdas = 1 + (2 -3) * (4 / 5);
console.log("Result of pemdas operator" + pemdas);

// Increment and Decrement
let z = 1;
// Pre-fix Incrementation
++z
console.log(z); //2

// Post-fix Incrementation
// returns previous value of the variable and 1 on its actual actual value
z++
console.log(z); //2

console.log(z++); //3
console.log(z); //4

console.log(++z); //5 - the new value is returned immediately

// Pre-fix Decrementation and Post-fix Decrementation
console.log(--z); // pre-fix
console.log(z--); // post-fix
console.log(z);


// Type Coercion
// is the automatic or implicit conversion of values from one data type to another

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);
// Adding/Concatenating a string and a number will result to a string

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
// The result is a number

let numE = true + 1;
console.log(numE);
console.log(typeof numE);
// the result is a number
// boolean "true" is associated with the value of 1

let numF = false + 1;
console.log	(numF)
// boolean "false" is associated with the value of 0

// Comparison operators
// (==) Equality Operator
let juan = 'juan';
console.log("Equality Operator")
console.log (1==1); //true
console.log (1==2); //false
console.log (1=='1'); //true
console.log (0==false); //true
console.log ('juan' == 'JUAN'); //false - case sensitive
console.log ('juan' == juan); //true

//(===) Strict Equality Operator
console.log("Strict Equality Operator")
console.log (1===1); //true
console.log (1===2); //false
console.log (1==='1'); //false
console.log (0===false); //false
console.log ('juan' === 'JUAN'); //false - case sensitive
console.log ('juan' === juan); //true

//(!=) Inequality Operator
console.log("Inequality Operator")
console.log (1!=1); //flase
console.log (1!=2); //true
console.log (1!='1'); //false
console.log (0!=false); //flase
console.log ('juan' != 'JUAN'); //true - case sensitive
console.log ('juan' != juan); //false 

// (!==)Strict Inequality Operator
console.log("Strict Inquality Operator")
console.log (1!==1); //false
console.log (1!==2); //true
console.log (1!=='1'); //true
console.log (0!==false); //true
console.log ('juan' !== 'JUAN'); //true - case sensitive
console.log ('juan' !== juan); //false

// Relational Comparison Operator
let x = 500;
let y = 700;
let w = 8000;
let numString = "5500";

// Greater Than (>)
console.log("Greater Than");
console.log(x > y); //false

// Less Than (<)
console.log("Less Than");
console.log(y<y); //false
console.log(numString < 6000); //true - force coercion to change string to a number
console.log(numString < 1000); //false

// Greater than or Equal (>=)

console.log(w>=w); //true

// Less than or Equal (>=)

console.log(y<=y); //true

//Logical Operators (&&, ||, !)

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

// Logical AND Operator (&& - Double Ampersand)
console.log("Logical AND Operator");
// return true if All operands are true
let	authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);

let requireLevel = 95;
let requireAge = 80;

let authorization3 = isRegistered && requireLevel ===95 && isLegalAge;
console.log(authorization3); //true

// Logical OR Operator (|| - Double Pipe)
// return true if atleast 1 of the operand are true.
console.log	("OR operator");

let userLevel = 100;
let userLevel2 = 65;
let userAge = 15;

let guildRequirement1 = isRegistered || userLevel2 >= requireLevel || userAge >= requireAge;
console.log(guildRequirement1); //true

// NOT Operator (!)
console.log("Not Operator");
// turns a boolean into the opposite value

let opposite = !isAdmin;
console.log(opposite); //true - is Admin original value is false
console.log(!isRegistered); //false

let guildAdmin = !isAdmin || userLevel2 >= requireLevel;
console.log(guildAdmin);

// CONDITTIONAL STATEMENT
// if else if and if statement

// if statement
// execute a statement ia a specified condition is true
if(true) {
	console.log("We just run an if condition!")
}

let numG = 5;

if(numG < 10) {
	console.log('Hello');
}

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if (userName3.length >= 10 && isRegistered && isAdmin) {
	console.log ("Welcome to Game Online!")
} else	{
	console.log("You are not ready")
}

//.length is a property of strings which determine the number of characters in the string.

//else statement
// execute a statement if all oher condition are false

if (userName3.length >=10 && userLevel3 >= requireLevel && userAge3 >= requireAge) {
	console.log("Thank you for joining the noobies Guild!")
} else {
	console.log("You are too strong to be a noob")
}

// else if statement
// execute a statement if previous condition are false

if (userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requireAge) {
console.log("Welcome noob")
} else if (userLevel3 > 25) {
	console.log ("you are too strong")
} else if (userAge3 <requireAge) {
	console.log("You are too young to join the guild")
} else if (userName3.length < 10) {
	console.log("Username too short")
} else {
	console.log("you are not ready")
};

// if, else if and else statement with function
let n = 3;
console.log(typeof n);
function addNum(num1, num2) {
	//check if the numbers being passed as an argument are number types
	if (typeof num1 === "number" && typeof num2 === "number") {
		console.log("Run only if both arguments passed are number types")
		console.log(num1 + num2)
	} else {
		console.log("One or both of the arguments are not number")
	}
};

addNum (5,"2");

//create a login function

//check if the username and password is a string type

/*function login(username, password){
	if (typeof username === "string" && typeof password === "string") {
		console.log("Both Arguments are string") 

		if (username.length >=8 && password.length >= 8) {
			console.log("Thank you for Logging in")
		} else if (username.length < 8) {
			alert ("username is too short")
		} else if (password.length < 8) {
			alert ("password too short")
		} else {
			alert ("Credentials too short")
		}
	} else {
		console.log("one or both argument is not a string")
	} 
}	

login ("jane","jane123")*/

// function with return keyword
let message = "No message.";
console.log(message)

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30) {
		return 'Not a typhoon yet';
	}
	else if (windSpeed <=61) {
		return 'Tropical depression detected.';
	}
	else if(windSpeed >=62 && windSpeed <=88){
		return 'Severe Tropical depression detected.';
	}
	else if (windSpeed >= 89 && windSpeed <= 117){
		return 'Severe tropical storm detected';
	}
	else {
		return 'typhoon detected'
	}
}

message = determineTyphoonIntensity(63);
console.log(message);

if (message == 'Severe tropical storm detected') {
	console.warn(message);
}

//console.warn is a good way to print warning in our console that could help us developer act on a certain output within out code


// truthy and falsy
// false(undefined, null, "", NaN, -0)
if (0) {
	console.log("Truthy")
}

let fName = "jane";
let mName = "doe";
let lName = "smith";

console.log(fName +" "+ mName+" "+lName);
// template literals(ES6)
console.log(`${fName} ${mName} ${lName}`);
// Ternary Operator (ES6)
/*
Syntax:
	(expression/condition) ? ifTrue : ifFalse;
	expression/condition ? ifTrue : ifFalse;

*/
// Single statement execution 

let ternaryResult = (1 < 18) ? true : false;
console.log(ternaryResult);

/*function
if (1 < 18) {
	return true
} else {
	return false
}*/


5000 > 1000 ? console.log("price is over 1000") : console.log("Price is less than 1000")
// else if with ternary operator
let a = 7;


a === 5
? console.log("A")
: (a === 10 ? console.log ("A is 10") : console.log ("A is not 5 or 10"))

// multiple statement execution
let name;
/*
function isOfLegalAge() {
	name = "john";
	return "You are of the legal age limit"
}

function inUnderAge() {
	name = "Jane";
	return "You are under the age limit"
}

let age = parseInt(prompt ("What is your age?"));
console.log(age)

let legalAge = (age > 18) ? isOfLegalAge() : inUnderAge();
console.log(`Result of the ternary operator in finction: ${legalAge}, ${name}`);
*/

// Switch Statement
/*
	-Can be used as an alternative to an if - else if - else statement.
	-syntax:
		switch(expresion) {
			case value1:
				statement;
				break;
			case value2:
				statement;
				break;
			.
			.
			.
			.
			case valuen:
				statement;
				break;
			default:
				statement;
		}
*/

/*let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day) {
	case 'monday':
		console.log("The color of the day is red");
		break;

	case 'tuesday':
		console.log("The color of the day is orange");
		break;

	case 'wednesday':
		console.log("The color of the day is yellow");
		break;

	case 'thursday':
		console.log("The color of the day is green");
		break;

	case 'friday':
		console.log("The color of the day is blue");
		break;

	case 'saturday':
		console.log("The color of the day is indigo");
		break;

	case 'sunday':
		console.log("The color of the day is violet");
		break;

	default:
		console.log("Please input a valid day")
}

*/
// Try-Catch-Finally Statement

function showIntensityAlert(windSpeed) {
	try{
		// Attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch(error) {
		console.log(typeof error);
		console.log(error.message);
	}
	finally {
		// Continue to execute code regardless of success or failure of code execution in the try block
		alert('Intensity updates will show new alert')
	}
}

showIntensityAlert(56);